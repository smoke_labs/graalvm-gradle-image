FROM debian:bookworm
RUN apt-get update && apt-get install unzip zip curl gcc zlib1g-dev -y

# gradle
RUN mkdir /opt/gradle
RUN curl -L "https://services.gradle.org/distributions/gradle-8.6-bin.zip" -o /tmp/gradle-8.6-bin.zip
RUN unzip -d /opt/gradle /tmp/gradle-8.6-bin.zip
ENV PATH="${PATH}:/opt/gradle/gradle-8.6/bin"

# graalvm
RUN mkdir /opt/graalvm
RUN curl -L "https://github.com/graalvm/graalvm-ce-builds/releases/download/jdk-21.0.2/graalvm-community-jdk-21.0.2_linux-x64_bin.tar.gz" -o /tmp/graalvm.tar.gz
RUN tar -xzf /tmp/graalvm.tar.gz -C /tmp/
RUN mv /tmp/graalvm-community-openjdk-21.0.2+13.1/* /opt/graalvm
ENV PATH="${PATH}:/opt/graalvm/bin"
ENV JAVA_HOME=/opt/graalvm
ENV GRAALVM_HOME=/opt/graalvm
